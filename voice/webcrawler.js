const request = require('request');
const cheerio = require('cheerio');
var convert = require('../lib/convert');
const voiceupdate = require('../lib/voiceupdate');
var mongojs = require('../configDB/db');
db = mongojs.connect;




exports.getWebCrawler = async function (url1, domain1, index_id, timetostamp) {

  try {
    // var agentR = voiceupdate.getAgent(); 

    // voiceupdate.proxyData("bing", function (proxyData) {
      var domain = ""
      if ((domain1) && typeof domain1 !== 'undefined' && domain1 !== null) {
        domain = domain1;
      } else {
        domain = extractHostname(url1)
      }

      const urlland = url1;
      var jar = request.jar();

      // let host = proxyData[0];
      // let user = proxyData[1];
      // let password = proxyData[2];
      // var proxyUrl = "http://" + user + ":" + password + "@" + host;
      // var proxiedRequest = request.defaults({ 'proxy': proxyUrl });
      setTimeout(() => {
        request({
          method: 'GET',
          url: urlland,
          headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*',
            'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'
          },
          jar: jar
        },async function (err, response, body) {
          try{
            if (!(err)&&response.statusCode==200) {
              let $ = cheerio.load(body);
              let metakeyword = $('meta[name=keywords]').attr('content');
              if (Array.isArray(metakeyword)) {
                metakeyword = metakeyword[0]
              }
              let title = $('title').text().trim();
              if (title === null || title === "" || typeof title ==="undefined") {
                title = urlland
              }
              let bodyall = $('body').text().trim();
              if (bodyall !== null && bodyall !== "" && typeof bodyall !=="undefined") {
                bodyall = bodyall.substr(0, 5000)
              }else{
                bodyall = ""
              }
              let h1 = $('h1').text().trim();
              if (h1 === null || h1 === "" || typeof h1 ==="undefined") {
                h1 = urlland
              }
    
    
              await db.collection("voice_web").find({ "voice_message": title }).toArray(async function (err, resultcheck) {
                if (resultcheck.length > 0) {
    
                  await db.collection("voice_web").update(
                    {
                      "_id": resultcheck._id,
                    },
                    {
                      $set: {
                        "_id": resultcheck._id,
                        "voice_refid": "",
                        "index_id": index_id,
                        "voice_keyword": metakeyword,
                        "voice_message": title,
                        "voice_body": bodyall,
                        "voice_h1": h1,
                        "domain": domain,
                        "url": urlland,
                        "collectdata": convert.dateFormat(),
                        "source_type": 'website'
                      }
                    },
                    function (err, rs) {
                      if (err) {
                        console.error(err);
                      }
                    });
    
                } else {
    
    
                  await db.collection("zprimarykey_voice_web").findAndModify(
                    {
                      query: { _id: "indexid" },
                      update: { $inc: { seq: 1 } },
                      new: true
                    },
                    async function (err, result) {
                      var id = result.seq;
                      var content = [];
                      content.push({
                        _id: id,
                        voice_refid: "",
                        index_id: index_id,
                        voice_keyword: metakeyword,
                        voice_message: title,
                        voice_body: bodyall,
                        voice_h1: h1,
                        domain: domain,
                        url: urlland,
                        postdate: convert.dateFormat(),
                        postdate: convert.dateFormat(),
                        collectdata: convert.dateFormat(),
                        source_type: 'website'
                      });
    
    
                      await db.collection("voice_web").insert(content, function (err, res) {
                        if (err) {
                          console.log('insert voice_web :' + err);
                        }
    
                      });
    
    
                    });//ปิด run number mongo
    
    
                }
              });
    
              await db.collection("index_repo_campaign").update({ "_id": index_id }, {
                $set: { "readindex": 'Y', "readnexttime": timetostamp, "readindexdate": new Date() }
              });// หยิบแล้ว stamp กลับเป็น Y ทันที พร้อมเซทเวลาในการคลอเลออีกครั้ง
    
              // callback(null,"success")
    
            } else {
  
              await voiceupdate.readIndexChangeR(index_id, "NS : Web ERR From Request Http  index_id: " + index_id, 200);
              
            }
  
          }catch(err){
            await voiceupdate.readIndexChangeR(index_id, "NS : Err in Evaluate  index_id: " + index_id, 200);
            throw err.message
          }
          
  
        });//ปิด request
      }, 500);
     


      function extractHostname(url) {
        var hostname;
        //find & remove protocol (http, ftp, etc.) and get hostname

        if (url.indexOf("//") > -1) {
          hostname = url.split('/')[2];
        }
        else {
          hostname = url.split('/')[0];
        }

        //find & remove port number
        hostname = hostname.split(':')[0];
        //find & remove "?"
        hostname = hostname.split('?')[0];

        return hostname;
      }


    // });//ปิด proxy
  } catch (err) {
    //console.error(err)
    await voiceupdate.readIndexChangeR(index_id, "MAIN CATCH NS : WEB ERR "+err.message+" index_id: " + index_id, 200);

  }

}//ปิด export function