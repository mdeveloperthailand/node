const request = require('request');
const puppeteer = require('puppeteer');
var convert = require('../lib/convert');
const voiceupdate = require('../lib/voiceupdate');
var mongojs = require('../configDB/db');
var db = mongojs.connect;



var contIndexs = 0;
var dataForIndex = []
var browser;
// ค้นหาจากคีเวิด #hashtag ทำได้แค่ Count จำนวน
exports.getTagCount = function (callback) {
  var keyword = "เบียร์";
  const URL = "https://api.instagram.com/v1/tags/search?q=" + encodeURIComponent(keyword) + "&access_token=207566344.0ab3c23.b73dc828c4274364a14ba1155f4ba8da"
  var jar = request.jar();

  request({
    method: 'GET',
    url: URL,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'
    },
    jar: jar
  }, function (err, response, body) {

    callback(null, body)
  });

}

exports.getIndexInMedia = function (data) {

  (async () => {
    // Set up browser and page.
    var browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
    });



    for (var index = 0; index < data.length; index++) {
      var row = data[index];
      var url = row[0]
      var index_id = row[1]
      var timetostamp = row[2]

      const page = await browser.newPage();
      await page.setRequestInterception(true);
      page.on('request', (request) => {
        if (request.resourceType() === 'image') {
          // console.log("abort jpeg "+request.resourceType());

          request.abort();
        } else {

          // console.log("continue "+request.resourceType());
          request.continue();
        }
      });
      page.setViewport({ width: 1280, height: 540 });

      await page.waitFor(3000);
      // ล็อกอิิน
      // await page.goto("https://www.instagram.com/explore/tags/เครื่องสำอางค์แบรนด์เนมแท้")
      await page.goto(url)


      await page.waitFor(5000);
      // await page.waitForSelector('div.EZdmt');
      // await scrolled(page);
      // div.EZdmt

      var secPopTap = await page.$("div.EZdmt")

      if (secPopTap) {

        await page.evaluate((sel) => {
          document.querySelector(sel).remove();
        }, "div.EZdmt")
      }

      await page.waitFor(2000);
      var campaign_id = [];
      var campaign_set = [];
      await db.collection("index_repo_campaign").find({ "url": url }).toArray(function (err, result) {
        //
        for (const rs of result) {
          campaign_id = rs.campaign_id;

          db.collection("campaign_setkeyword").find({ 'campaign_id': rs.campaign_id }).toArray(function (err, result1) {
            for (const rs1 of result1) {
              var arrPush = rs1.campaign_id + "_" + rs1._id + "_" + rs1.typeof_keyword;

              campaign_set.push(arrPush);
            }
          });

        }
      });

      await page.waitFor(2000);

      await clickone(page);
      await page.waitFor(1000);
      const rs = await cliclNextPost(page, campaign_id, campaign_set, true);
      // await page.waitFor(3000);  

      // const rs = await page.evaluate(managesinglepost)
      await page.waitFor(1000);
      await memoryUsage();
      await browser.close();

      console.log("end instagram close page for TYPE PAGE");
      // callback(null,rs);
    }
  })();


  async function cliclNextPost(page, campaign_id, campaign_set, check) {
    try {
      if (check) {
        const clickbtn = await page.$$("a.HBoOv")
        if (clickbtn) {
          await clickbtn[0].click()
          await page.waitFor(1000);

          const rs = await page.evaluate(manageIndex)

          rs[0]["campaign_id"] = campaign_id;
          rs[0]["campaign_set"] = campaign_set;

          dataForIndex.push(rs);

          const rsCheckBrak = await page.evaluate(checkdate);
          console.log(contIndexs);
          console.log(rs[0]['url']);
          console.log(rsCheckBrak);

          if (!rsCheckBrak) {
            check = rsCheckBrak

            return dataForIndex
            //  break;
          } else {
            //  setTimeout(() => {
            await page.waitFor(200);

            contIndexs = contIndexs + 1;
            await cliclNextPost(page, campaign_id, campaign_set, check)
            // }, 1000);
          }
        } else {
          console.log("Not Post In Page url" + url);
          db.collection("index_repo_campaign").update({ "_id": index_id }, {
            $set: { "error_status": "F:No Data For evaluate", "readindex": 'Y', "readnexttime": timetostamp, "readindexdate": new Date() }
          });// หยิบแล้ว stamp กลับเป็น Y ทันที พร้อมเซทเวลาในการคลอเลออีกครั้ง

          return dataForIndex
        }
      }
    } catch (e) {
      console.log(e)
    }

  }

  async function clickone(page) {
    const clickbtn = await page.$$("div.v1Nh3")
    if (clickbtn) {
      await clickbtn[0].click('a')
      await page.waitFor(1000);
    }
  }


  async function checkdate() {
    var time = document.querySelector('time[datetime]')

    if (time) {
      time = time.attributes[1].value
      var d = new Date();
      var str = d.setDate(d.getDate() - 10)
      str = str.toString();
      var datestop = str.substring(0, 10)
      datestop = parseInt(datestop);

      var dateIG = new Date(time)
      var dateIGCon = dateIG.setDate(dateIG.getDate()).toString()
      var dateIGF = dateIGCon.substring(0, 10)
      if (parseInt(dateIGF) <= datestop) {
        return false
      }
    }
    return true
  }

  async function memoryUsage() {

    const used = process.memoryUsage();
    for (let key in used) {
      console.log("Instagram" + `${key} ${Math.round(used[key] / 1024 / 1024 * 100) / 100} MB`);
    }
    // await page.close();

    console.log("end instagram close page");
  }
  // campaign_id	
  // campaign_set	
  // title	
  // domain	
  // url	
  // platform	 instagram
  // platform_typepost	posts 

  async function manageIndex() {

    try {

      var passed = [];
      var keepArr = {};

      keepArr = {};
      // keepArr["campaign_id"] = "";
      // keepArr["campaign_set"] = "";
      keepArr["title"] = document.title;
      keepArr["domain"] = "instagram.com";
      keepArr["url"] = document.URL;
      keepArr["platform"] = "instagram";

      passed.push(keepArr)

      return passed;


    } catch (error) {
      console.log(error);
    }



  }
}

// ค้นหา comment ใน media แต่ละตัว
exports.getCommentInMedia = function (callback) {



  var a = convert.engagementToInt("9 like");
  var g = convert.engagementToInt("All 11");
  var e = convert.engagementToInt("100");
  var f = convert.engagementToInt("42,444");

  console.log("9.9 K " + a);
  console.log("All 11K" + g);
  //   console.log("90 M "+b);
  //   console.log("1.4 M "+c);
  //   console.log("10 m "+d);
  console.log("100 " + e);
  console.log("42,444 " + f);

  var str = "https://www.facebook.com/1164107963611361/photos/a.1164128126942678/1791457384209746/?type=1&theater"
  var c = str.match(/[a-zA-Z0-9]+/g);

  var sdd = "`https://www.facebook.com/Cosmetic20baht/photos/a.722679961076775/2212367742107982/?type=1&theater"
  var cd = sdd.match(/[a-zA-Z0-9]+/g);

  console.log(c[5]);
  console.log(cd[5]);


  var facebook = convert.dateFormat("Wednesday, October 24, 2018 at 9:19 PM", "FB");

  var facebookOld = convert.dateFormat("1544379066", "FB");

  console.log(facebook);
  console.log(facebookOld);
  // convert.dateFormat("2018-11-17T07:12:56.000Z","IG");


}




// puppeteer Get comment
exports.getCommentInMediaPup = function (data, numCount, rowx, checkBrowser, success_pro = 0, err_pro = 0) {

  var start_process = new Date()
  var success_process = success_pro;
  var err_process = err_pro;
  var end_point_process = new Date()
  var page
  var browser
  var agentR = voiceupdate.getAgent();

  voiceupdate.proxyData("instagram",function (proxyData) {
    (async () => {
      // let db = mongojs.connect;
      // Set up browser and page.
    

        if(proxyData == "NOT PROXY PLATFORM"){
          throw (proxyData)
        }
        getCommentInMediaPups(data, numCount, rowx, checkBrowser,proxyData);
      

    })();
  }); 

  async function getCommentInMediaPups(data, numCount, rowx, checkBrowser, proxyData,success_pro = 0, err_pro = 0){
    (async () => {
      // let db = mongojs.connect;
      // Set up browser and page.
      try {
 

      voiceupdate.d("sv start instagram")
      voiceupdate.d("total : " + data.length);
      if (checkBrowser) {
        browser = await puppeteer.launch({
          headless: true,
          args: [
            '--proxy-server=' + proxyData[0],
            '--no-sandbox',
            '--disable-setuid-sandbox']
        });

        page = await browser.newPage();
        // await page.setUserAgent(agentR);
        await page.authenticate({ username: proxyData[1], password: proxyData[2] });

        // page.on('console', consoleObj => console.log(consoleObj.text));

        page.setViewport({ width: 1280, height: 540 });
        await page.setRequestInterception(true);
        page.on('request', (request) => {
          if (request.resourceType() === 'css') {

            request.abort();
          } else {
            request.continue();
          }
        });
      }

      // await page.goto("https://search.yahoo.com/search?p=%E0%B8%AD%E0%B8%B0%E0%B9%84%E0%B8%AB%E0%B8%A5%E0%B9%88%E0%B8%A2%E0%B8%99%E0%B8%95%E0%B9%8C")

      // return false


      // console.log("start instagram");

    
        for (var index = rowx; index < data.length; index++) {
          var row = data[index];
          var url = row[0]
          var index_id = row[1]
          var timetostamp = row[2]
          // async function opentap(url,index_id,timetostamp){

          await page.waitFor(3000);
          // ล็อกอิิน
          // await page.goto("https://www.instagram.com/p/BqRcuulBTPL/")
          // console.log("start url :"+url)

          voiceupdate.d("----- No" + (index + 1) + " : get start url :" + url)
          await page.goto(url)


          // await page.waitForSelector('a.zV_Nj');

          await page.waitFor(4000);
          try {
            await clickone(page);
          } catch (err) {
            voiceupdate.d("     Click No Comment")
          }
          await page.waitFor(5000);
          var campaign_id = [];
          var campaign_set = [];
          await db.collection("index_repo_campaign").find({ "url": url }).toArray(function (err, result) {
            //
            for (const rs of result) {
              campaign_id = rs.campaign_id;

              db.collection("campaign_setkeyword").find({ 'campaign_id': rs.campaign_id }).toArray(function (err, result1) {
                for (const rs1 of result1) {
                  var arrPush = rs1.campaign_id + "_" + rs1._id + "_" + rs1.typeof_keyword;

                  campaign_set.push(arrPush);
                }
              });
            }
          });

          await page.waitFor(5000);

          try {
            await addpost(page, index_id, timetostamp, campaign_id, campaign_set, url)
          } catch (err) {
            throw ("Add PORT " + err)
          }

          try {
            await addcomment(page, index_id, timetostamp, campaign_id, campaign_set, url)
          } catch (err) {
            throw ("Add Comment " + err)
          }

          // voiceupdate.d("     evaluate data")
          // const rs = await page.evaluate(managesinglepost)
          // await page.waitFor(1000);
          // // console.log(rs.length);
          // db.collection("voice_instagram").remove(
          //   {
          //     "index_id": index_id
          //   });
          // if (rs) {

          //   voiceupdate.d("     addMongo")
          //   await addMongo(index_id, rs, timetostamp, campaign_id, campaign_set, url, 0);
          //   success_process++
          // } else {

          //   voiceupdate.d("     No Data For evaluate :" + url)
          //   voiceupdate.readIndexChangeR(index_id, "F:No Data For evaluate")
          //   // err_process++
          // }

          voiceupdate.showMemoryUsage("IG ")

        }
        // console.log("end loop "+numCount);  

        voiceupdate.d("----- end loop " + numCount)
        await browser.close();
        end_point_process = new Date()
        voiceupdate.dSum(success_process, err_process, start_process, end_point_process, data.length);

        voiceupdate.d("----- end instagram close browser")
        // console.log("end instagram close browser");
        // await db.close();

        // console.log("end db");
      } catch (err) {


        console.error("PP ERR : " + err);
        let senderr = 200
        var keeperr = err.toString()
        if (keeperr.includes("TimeoutError")) {
          senderr = 501
        }
        await voiceupdate.readIndexChangeR(index_id, "NS : PP ERR ig " + err + " index_id: " + index_id, senderr);
        err_process++
        index++;
        browser.close()

        await getCommentInMediaPups(data, numCount, index, true,proxyData, success_process, err_process)
      }

    })();
  }

  async function addpost(page, index_id, timetostamp, campaign_id, campaign_set, url) {
    voiceupdate.d("     evaluate data POST")
    try {
      const rs = await page.evaluate(managesinglepost)
      await page.waitFor(4000);
      // console.log(rs.length);
      db.collection("voice_instagram").remove(
        {
          "index_id": index_id,
          'typepost': 'post'
        });
      await page.waitFor(2000);
      voiceupdate.d("     addMongo")
      await page.waitFor(5000);
      await addMongo(index_id, rs, timetostamp, campaign_id, campaign_set, url, 0);
      success_process++
    } catch (error) {
      throw ("ERR: No Data For evaluate :" + url)
    }
  }


  async function addcomment(page, index_id, timetostamp, campaign_id, campaign_set, url) {

    voiceupdate.d("     evaluate data COMMENT")
    try {
      let indexref;

      await page.waitFor(4000);
      db.collection("voice_instagram").find({ index_id: index_id, 'typepost': 'post' }).toArray(function (err, resultd) {

        for (const row of resultd) {
          if (data[rowx]) {

            indexref = row._id
          } else {
            throw (" ig voice_refid error")
          }
        }

      });

      await page.waitFor(4000);
      await page.addScriptTag({ content: `${managesinglecomment}` });

      await page.waitFor(2000);
      const rs = await page.evaluate(({ indexref }) => {
        return managesinglecomment(indexref);
      }, { indexref });
      await page.waitFor(4000);

      db.collection("voice_instagram").remove(
        {
          "index_id": index_id,
          'typepost': 'comment'
        });

      voiceupdate.d("     addMongoComment")

      await page.waitFor(2000);
      await addMongoComment(index_id, rs, timetostamp, campaign_id, campaign_set, url, 0);
      // success_process++
    } catch (error) {
      throw (error + " No Data For evaluate :" + url)
    }
  }

  async function addMongo(index_id, data, timetostamp, campaign_id, campaign_set, url, rowx) {

    try {
      if ((data[rowx])) {

        // for (const row of data) {


        // console.log("addMongo");
        await db.collection("zprimarykey_voice_instagram").findAndModify(
          {
            query: { _id: "indexid" },
            update: { $inc: { seq: 1 } },
            new: true
          },
          function (err, result) {

            // console.log(data[rowx]);
            if (typeof data[rowx] !== "undefined") {
              // console.log("have data");
              // console.log("delete voice_instagram");

              data[rowx]["_id"] = result.seq
              var number = convert.engagementToInt("" + data[rowx]["likepost"])
              data[rowx]["likepost"] = number
              data[rowx]["postdate"] = convert.dateFormat(data[rowx]["postdate"], "ig");
              data[rowx]["collectdata"] = convert.dateFormat();
              data[rowx]["index_id"] = index_id;
              data[rowx]["directurl"] = url;
              data[rowx]["campaign_id"] = campaign_id;
              data[rowx]["campaign_set"] = campaign_set;
              data[rowx]["comment"] = convert.engagementToInt("" + data[rowx]["comment"])
              data[rowx]["postymd"] = convert.dateFormat('','POSTYMD');
              db.collection("voice_instagram").update(
                {
                  "voice_message": data[rowx].comment
                },
                {
                  $set: data[rowx]
                },
                { upsert: true });

              rowx++;
              // console.log(data[rowx]);


            } else {//ปิด if typyof
              console.log('not data for evlue');
            }//ปิด else typyof

          });  //ปิด findAndModify


        setTimeout(() => {
          addMongo(index_id, data, timetostamp, campaign_id, campaign_set, url, rowx)
        }, 1000);

      } else {
        await voiceupdate.readIndexChangeY(index_id, timetostamp)
      }

    } catch (err) {
      // console.error(err);
      throw (err + "Add to mogodb for :" + index_id)
      // await voiceupdate.readIndexChangeR(index_id, "Add to mogodb for :" + index_id)
      // db.collection("index_repo_campaign").update({"_id":index_id},{
      // $set:{"error_status":"Add to mogodb for :"+index_id,"readindex" : 'Y',"readnexttime" : timetostamp,"readindexdate" : new Date()}});// หยิบแล้ว stamp กลับเป็น Y ทันที พร้อมเซทเวลาในการคลอเลออีกครั้ง

    }

  }

  async function addMongoComment(index_id, data, timetostamp, campaign_id, campaign_set, url, rowx) {

    try {
      if ((data[rowx])) {

        // for (const row of data) {


        // console.log("addMongoComment");
        await db.collection("zprimarykey_voice_instagram").findAndModify(
          {
            query: { _id: "indexid" },
            update: { $inc: { seq: 1 } },
            new: true
          },
          function (err, result) {

            // console.log(data[rowx]);
            if (typeof data[rowx] !== "undefined") {
              // console.log("have data");
              // console.log("delete voice_instagram");

              data[rowx]["_id"] = result.seq
              var number = convert.engagementToInt("" + data[rowx]["likepost"])
              data[rowx]["likepost"] = number
              data[rowx]["postdate"] = convert.dateFormat(data[rowx]["postdate"], "ig");
              data[rowx]["collectdata"] = convert.dateFormat();
              data[rowx]["comment"] = convert.engagementToInt("" + data[rowx]["comment"])
              data[rowx]["index_id"] = index_id;
              data[rowx]["directurl"] = url;
              data[rowx]["campaign_id"] = campaign_id;
              data[rowx]["campaign_set"] = campaign_set;
              data[rowx]["postymd"] = convert.dateFormat('','POSTYMD');

              db.collection("voice_instagram").update(
                {
                  "voice_message": data[rowx].comment
                },
                {
                  $set: data[rowx]
                },
                { upsert: true });

              rowx++;
              // console.log(data[rowx]);


            } else {//ปิด if typyof
              console.log('not data for evlue');
            }//ปิด else typyof

          });  //ปิด findAndModify


        setTimeout(() => {
          addMongo(index_id, data, timetostamp, campaign_id, campaign_set, url, rowx)
        }, 1000);

      } else {
        await voiceupdate.readIndexChangeY(index_id, timetostamp)
      }

    } catch (err) {
      // console.error(err);
      throw (err + "Add to mogodb for :" + index_id)
      // await voiceupdate.readIndexChangeR(index_id, "Add to mogodb for :" + index_id)
      // db.collection("index_repo_campaign").update({"_id":index_id},{
      // $set:{"error_status":"Add to mogodb for :"+index_id,"readindex" : 'Y',"readnexttime" : timetostamp,"readindexdate" : new Date()}});// หยิบแล้ว stamp กลับเป็น Y ทันที พร้อมเซทเวลาในการคลอเลออีกครั้ง

    }

  }

  async function clickone(page) {
    var clickbtn = await page.$$("button.Z4IfV")
    if (clickbtn) {
      if (clickbtn.length > 0) {
        for (const doit of clickbtn) {
          await doit.click('button.Z4IfV')
          await page.waitFor(1000);
        }
        clickone(page);
      }
    }else{
      return false
    }
    
  }




  // async function managesinglepost(url,index_id,campaign_set,campaign_id) {
  async function managesinglepost() {

    try {

      var passed = [];
      var keepArr = {};
      var mainmedie = document.querySelector('img.FFVAD').attributes[5].value;
      var ownerpost = document.querySelector('.FPmhX.notranslate.TlrDj');
      var postmain = document.querySelector('.C4VMK span');
      var usercommentall = document.querySelectorAll('.FPmhX.notranslate.TlrDj');
      var commentall = document.querySelectorAll('.C4VMK span');
      var timepost = document.querySelector('time[datetime]');
      var likepost = document.querySelector('a.zV_Nj'); //ถ้าไม่ login จะไม่ได้ like


      var countComment = commentall.length - 1
      if (!likepost) {
        likepost = document.querySelector('span.vcOH2');
      }
      if (!mainmedie) {

        mainmedie = document.querySelector('video.tWeCl').attributes[2].value;
      }
      var postcom = "post";



      keepArr = {};
      keepArr["voice_refid"] = "";
      keepArr["image"] = mainmedie;
      // keepArr["ownerpost"] = ownerpost.innerText;
      // keepArr["voice_post"] = postmain.innerText;
      keepArr["postdate"] = timepost.attributes[1].value;
      keepArr["likepost"] = likepost.innerText;//ถ้าไม่ login จะไม่ได้ like
      keepArr["author"] = usercommentall[0].innerText;
      keepArr["voice_message"] = commentall[0].innerText;
      keepArr["comment"] = countComment;
      keepArr["typepost"] = postcom;
      keepArr["location_lat"] = "";
      keepArr["location_long"] = "";
      keepArr["location_name"] = "";
      keepArr["source_type"] = "instagram";

      passed.push(keepArr);



      return passed;


    } catch (error) {
      throw (error)

    }
  }


  // async function managesinglecomment(db,index_id, timetostamp, campaign_id, campaign_set, url, rowx) {
  async function managesinglecomment(indexref) {

    try {

      var passed = [];
      var keepArr = {};
      var mainmedie = document.querySelector('img.FFVAD').attributes[5].value;
      var ownerpost = document.querySelector('.FPmhX.notranslate.TlrDj');
      var postmain = document.querySelector('.C4VMK span');
      var usercommentall = document.querySelectorAll('.FPmhX.notranslate.TlrDj');
      var commentall = document.querySelectorAll('.C4VMK span');
      var timepost = document.querySelector('time[datetime]');
      var likepost = document.querySelector('a.zV_Nj'); //ถ้าไม่ login จะไม่ได้ like

      if (!likepost) {
        likepost = document.querySelector('span.vcOH2');
      }
      if (!mainmedie) {

        mainmedie = document.querySelector('video.tWeCl').attributes[2].value;
      }
      // var i = 1;
      postcom = "comment";

      // for (const rs of usercommentall) {
      var countComment = commentall.length - 1
      for (let index = 1; index < usercommentall.length; index++) {

        if (!indexref) {
          indexref = 1111
        }
        keepArr = {};
        keepArr["voice_refid"] = indexref;
        keepArr["image"] = mainmedie;
        // keepArr["ownerpost"] = ownerpost.innerText;
        // keepArr["voice_post"] = postmain.innerText;
        keepArr["postdate"] = timepost.attributes[1].value;
        keepArr["likepost"] = likepost.innerText;//ถ้าไม่ login จะไม่ได้ like
        keepArr["author"] = usercommentall[index].innerText;
        keepArr["voice_message"] = commentall[index].innerText;
        keepArr["comment"] = countComment;
        keepArr["typepost"] = postcom;
        keepArr["location_lat"] = "";
        keepArr["location_long"] = "";
        keepArr["location_name"] = "";
        keepArr["source_type"] = "instagram";

        passed.push(keepArr);
        // i++;
        // await addMongoComment(db,index_id, passed, timetostamp, campaign_id, campaign_set, url, rowx)
      }


      return passed;


    } catch (error) {
      throw (error)

    }
  }


}
