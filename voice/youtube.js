const request = require('request');
var mongojs = require('../configDB/db');
var db = mongojs.connect;
var convert = require('../lib/convert');
var voiceupdate = require('../lib/voiceupdate');


// getCommentThread by videoid
exports.getCommentThread = async function (data, rows) {
  var idposttocomment = 786
  // var agentR = voiceupdate.getAgent(); 
  await voiceupdate.proxyData("youtube",async function (proxyData) {


  var start_process = new Date()
  var success_process = 0;
  var err_process = 0;
  var end_point_process = new Date()


  voiceupdate.d("sv start youtube")
  voiceupdate.d("total : " + data.length);
  for (var index = rows; index < data.length; index++) {
    var row = data[index];


    var fullurl = row[0]
    const index_id = row[1]
    var videoid = row[2]
    var timetostamp = row[3]

    //post

    try {
      await voiceupdate.d("----- No" + (index + 1) + " : get start url :" + fullurl)
      await voiceupdate.d("      START REQUEST MAIN POST " + fullurl)
      await voiceupdate.d("      PROGRESSING");

      var URL = "https://www.googleapis.com/youtube/v3/videos?id=" + videoid + "&maxResults=1&part=snippet,contentDetails,statistics&key=AIzaSyAUg-HrJST7PlCjnS0Na9ZzJMouvfMF4F4"
      var jar = request.jar();
      let host = proxyData[0];
      let user = proxyData[1];
      let password = proxyData[2];
      var proxyUrl = "http://" + user + ":" + password + "@" + host;
      var proxiedRequest = request.defaults({ 'proxy': proxyUrl });
      await proxiedRequest({
        method: 'GET',
        url: URL,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': '*',
          'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'
        },
        jar: jar
      },async function (err, response, body) {
        // จัดอาเรย์ลงมองโก

        try {

          if (typeof body !== "undefined") {
            var data = JSON.parse(body)
            var mains = data.items;
          }




          //SELECT CAMPAIGN
          await db.collection("index_repo_campaign").find({ 'url': fullurl }).toArray(async function (err, result) {
            if (err) {
              console.error("select cam :" + err);
            }
            var campaign_id = [];
            var campaign_set = [];
            for (const rs of result) {

              campaign_id.push(rs.campaign_id);

              await db.collection("campaign_setkeyword").find({ 'campaign_id': rs.campaign_id }).toArray(async function (err, result1) {
                if (err) {
                  console.error("select setkey :" + err);
                }
                for (const rs1 of result1) {
                  var arrPush = rs1.campaign_id + "_" + rs1._id + "_" + rs1.typeof_keyword;
                  // console.log(arrPush); 

                  campaign_set.push(arrPush);
                }


              });
            }
            // setTimeout(() => {
              await manageCampaign(campaign_id, mains, campaign_set, index_id, timetostamp);
            // }, 500);

            // success_process++


          });
          //CLOSE SELECT CAMPAIGN

        } catch (err) {
          db.collection("index_repo_campaign").update({ "_id": index_id }, {
            $set: { "error_status": "NS", "readindex": 'Y', "readnexttime": timetostamp, "readindexdate": new Date() }
          });// หยิบแล้ว stamp กลับเป็น Y ทันที พร้อมเซทเวลาในการคลอเลออีกครั้ง
        }




        async function manageCampaign(rscampaign, mains, campaign_set, index_id, timetostamp) {

          try {

            await db.collection("voice_youtube").find({ "voice_message": mains[0].snippet.title }).toArray(async function (err, resultcheck) {
              if (resultcheck.length > 0) {
                idposttocomment = resultcheck._id
                await db.collection("voice_youtube").update(
                  {
                    "_id": resultcheck._id,
                  },
                  {
                    $set: {
                      "_id": resultcheck._id,
                      "voice_refid": 0,
                      "index_id": index_id,
                      "channel": "https://www.youtube.com/channel/" + mains[0].snippet.channelId,
                      "campaign_id": rscampaign,
                      "campaign_set": campaign_set,
                      "voice_message": mains[0].snippet.title,
                      "directurl": "https://www.youtube.com/watch?v=" + mains[0].id,
                      "author": mains[0].snippet.channelTitle,
                      "authorimage": mains[0].snippet.thumbnails.medium.url,
                      "like": mains[0].statistics.likeCount,
                      "dislike": mains[0].statistics.dislikeCount,
                      "view": mains[0].statistics.viewCount,
                      "subscribers": "",
                      "engagement": parseInt(mains[0].statistics.likeCount) + parseInt(mains[0].statistics.dislikeCount) + parseInt(mains[0].statistics.viewCount),
                      "postdate": convert.dateFormat(mains[0].snippet.publishedAt, "YOUTUBE"),
                      "postymd": convert.dateFormat(mains[0].snippet.publishedAt, "POSTYMD"),
                      "collectdate": convert.dateFormat(),
                      "collectnexttime": timetostamp,
                      "source_type": 'youtube',
                      "typepost": 'posts'
                    }
                  },
                  async function (err, rs) {
                    if (err) {
                      console.error("update post youtube err : " + err);
                    }
                  });

              } else {


                await db.collection("zprimarykey_voice_youtube").findAndModify(
                  {
                    query: { _id: "indexid" },
                    update: { $inc: { seq: 1 } },
                    new: true
                  },
                  async function (err, result) {
                    var id = result.seq;
                    idposttocomment = result.seq;
                    var content = [];
                    content.push({
                      _id: id,
                      voice_refid: 0,
                      index_id: index_id,
                      channel: "https://www.youtube.com/channel/" + mains[0].snippet.channelId,
                      campaign_id: rscampaign,
                      campaign_set: campaign_set,
                      voice_message: mains[0].snippet.title,
                      directurl: "https://www.youtube.com/watch?v=" + mains[0].id,
                      author: mains[0].snippet.channelTitle,
                      authorimage: mains[0].snippet.thumbnails.medium.url,
                      like: mains[0].statistics.likeCount,
                      dislike: mains[0].statistics.dislikeCount,
                      view: mains[0].statistics.viewCount,
                      subscribers: "",
                      engagement: parseInt(mains[0].statistics.likeCount) + parseInt(mains[0].statistics.dislikeCount) + parseInt(mains[0].statistics.viewCount),
                      postdate: convert.dateFormat(mains[0].snippet.publishedAt, "YOUTUBE"),
                      postymd: convert.dateFormat(mains[0].snippet.publishedAt, "POSTYMD"),
                      collectdate: convert.dateFormat(),
                      collectnexttime: timetostamp,
                      source_type: 'youtube',
                      typepost: 'posts'
                    });

                    await db.collection("voice_youtube").insert(content,async function (err, res) {
                      if (err) {
                        console.log('insert voice_youtube Err :' + err);
                      }

                    });


                  });//ปิด run number mongo


              }
            });



          } catch (err) {
            // console.error("manage addmon POST ERR : "+err.message)
            await db.collection("index_repo_campaign").update({ "_id": index_id }, {
              $set: { "error_status": "F", "readindex": 'Y', "readnexttime": timetostamp, "readindexdate": new Date() }
            });// หยิบแล้ว stamp กลับเป็น Y ทันที พร้อมเซทเวลาในการคลอเลออีกครั้ง
          }

          await voiceupdate.readIndexChangeY(index_id, timetostamp)//Stamp Y

        }// ปิด manageCampaign

       



      });//ปิด REQUEST



    } catch (err) {//try post ใหญ่สุด
      console.error("REQUEST POST ERR : " + err);
      end_point_process++;
      await db.collection("index_repo_campaign").update({ "_id": index_id }, {
        $set: { "error_status": "NS", "readindex": 'Y', "readnexttime": timetostamp, "readindexdate": new Date() }
      });// หยิบแล้ว stamp กลับเป็น Y ทันที พร้อมเซทเวลาในการคลอเลออีกครั้ง
    }









    //comment

    try {

      var URL = "https://www.googleapis.com/youtube/v3/commentThreads?videoId=" + videoid + "&maxResults=100&part=snippet,replies&key=AIzaSyAUg-HrJST7PlCjnS0Na9ZzJMouvfMF4F4"
      var jar = request.jar();
      await request({
        method: 'GET',
        url: URL,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': '*',
          'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'
        },
        jar: jar
      },async function (err, response, body) {
        // จัดอาเรย์ลงมองโก

        if (typeof body !== "undefined") {
          var data = JSON.parse(body)
          var mains = data.items;
        }

        //SELECT CAMPAIGN
        await db.collection("index_repo_campaign").find({ 'url': fullurl }).toArray(async function (err, result) {
          var campaign_id = [];
          var campaign_set = [];
          for (const rs of result) {

            campaign_id.push(rs.campaign_id);

            await db.collection("campaign_setkeyword").find({ 'campaign_id': rs.campaign_id }).toArray(async function (err, result1) {
              for (const rs1 of result1) {
                var arrPush = rs1.campaign_id + "_" + rs1._id + "_" + rs1.typeof_keyword;
                // console.log(arrPush);

                campaign_set.push(arrPush);
              }


            });
          }
          // setTimeout(() => {
            await manageCampaign(campaign_id, mains, campaign_set, index_id, timetostamp, 0);
          // }, 5000);

        });
        //CLOSE SELECT CAMPAIGN




        async function manageCampaign(rscampaign, mains, campaign_set, index_id, timetostamp, rowx) {

          try {
            if ((mains[rowx])) {
             
              await db.collection("voice_youtube").find({ "voice_message": mains[rowx].snippet.topLevelComment.snippet.textOriginal }).toArray(async function (err, resultcheck) {
                if (resultcheck.length > 0) {

                  await db.collection("voice_youtube").update(
                    {
                      "_id": resultcheck._id,
                    },
                    {
                      $set: {
                        '_id': resultcheck.id,
                        'voice_refid': idposttocomment,
                        'index_id': index_id,
                        'channel': mains[rowx].snippet.topLevelComment.snippet.authorChannelUrl,
                        'campaign_id': rscampaign,
                        'campaign_set': campaign_set,
                        'voice_message': mains[rowx].snippet.topLevelComment.snippet.textOriginal,
                        'directurl': "https://www.youtube.com/watch?v=" + mains[rowx].snippet.topLevelComment.snippet.videoId,
                        'author': mains[rowx].snippet.topLevelComment.snippet.authorDisplayName,
                        'authorimage': mains[rowx].snippet.topLevelComment.snippet.authorProfileImageUrl,
                        'like': mains[rowx].snippet.topLevelComment.snippet.likeCount,
                        'dislike': "",
                        'view': "",
                        'subscribers': "",
                        'engagement': "",
                        'postdate': convert.dateFormat(mains[rowx].snippet.topLevelComment.snippet.publishedAt, "YOUTUBE"),
                        'postymd': convert.dateFormat(mains[rowx].snippet.topLevelComment.snippet.publishedAt, "POSTYMD"),
                        'collectdate': convert.dateFormat(),
                        'collectnexttime': timetostamp,
                        'source_type': 'youtube',
                        "typepost": 'comment'
                      }
                    },
                    async function (err, rs) {
                      if (err) {
                        console.error("update comment yb err : " + err);
                      }
                      rowx++;
                      await manageCampaign(rscampaign, mains, campaign_set, index_id, timetostamp, rowx)
                    });

                } else {

                  await db.collection("zprimarykey_voice_youtube").findAndModify(
                    {
                      query: { _id: "indexid" },
                      update: { $inc: { seq: 1 } },
                      new: true
                    },
                    async function (err, result) {
                      var id = result.seq;
                      var content = [];
                      content.push({
                        _id: id,
                        voice_refid: idposttocomment,
                        index_id: index_id,
                        channel: mains[rowx].snippet.topLevelComment.snippet.authorChannelUrl,
                        campaign_id: rscampaign,
                        campaign_set: campaign_set,
                        voice_message: mains[rowx].snippet.topLevelComment.snippet.textOriginal,
                        directurl: "https://www.youtube.com/watch?v=" + mains[rowx].snippet.topLevelComment.snippet.videoId,
                        author: mains[rowx].snippet.topLevelComment.snippet.authorDisplayName,
                        authorimage: mains[rowx].snippet.topLevelComment.snippet.authorProfileImageUrl,
                        like: mains[rowx].snippet.topLevelComment.snippet.likeCount,
                        dislike: "",
                        view: "",
                        subscribers: "",
                        engagement: "",
                        postdate: convert.dateFormat(mains[rowx].snippet.topLevelComment.snippet.publishedAt, "YOUTUBE"),
                        postymd: convert.dateFormat(mains[rowx].snippet.topLevelComment.snippet.publishedAt, "POSTYMD"),
                        collectdate: convert.dateFormat(),
                        collectnexttime: timetostamp,
                        source_type: 'youtube',
                        typepost: 'comment'
                      });


                      await db.collection("voice_youtube").insert(content,async function (err, res) {
                        if (err) {
                          console.log('insert comment voice_youtube Err :' + err);
                        }
                        rowx++;
                        await manageCampaign(rscampaign, mains, campaign_set, index_id, timetostamp, rowx)

                      });


                    });//ปิด run number mongo


                }
              });



            }//ปิด if main[rowx]

          } catch (err) {
            // console.log("NO. "+index+" ERR "+fullurl)
          }


        }// ปิด manageCampaign

      });//ปิด REQUEST

    } catch (err) {
      console.error("REQUEST Comment ERR : " + err);
    }




    await voiceupdate.d("----- END NO. " + index + " RETURN PROCESS Y TO INDEX " + index_id + " : COMPLETE")

 
  }//ปิด for ใหญ่สุด



  await voiceupdate.showMemoryUsage("youtube");

  await voiceupdate.d("----- end loop youtube") 
  end_point_process = new Date()
  await voiceupdate.dSum(success_process,err_process,start_process,end_point_process,data.length);

  await voiceupdate.d("----- end youtube")

  });
}
