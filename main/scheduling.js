var mongojs = require('../configDB/db');
var db = mongojs.connect;
var youtube = require('../voice/youtube')
var facebookcrawler = require('../voice/facebookcrawler')
var webcrawler = require('../voice/webcrawler')
var pantip = require('../voice/pantip')
var twitter = require('../voice/twitter')

var instagram = require('../voice/instagram')




exports.mainManage = function (model, callback) {


    try {
        var date_now = Date.now();//1542084181388
        db.collection("index_repo_campaign").find({ 'platform': model, 'status': 'Y', 'readindex': 'Y', 'readnexttime': { $lte: date_now } }).sort({ qc_score: -1, rereadindexdate: -1 }).limit(1000).toArray(function (err, result) {
            requestCrawler(result, 0, model);
            if (!result) {
                throw "NO INDEX RETURN FROM index_repo_campaign"
            }
        });
    } catch (err) {
        console.error("QUERY INDEX REPO CAMPAIGN : " + err.message);

    }

    async function updateStatus(data, rowx) {
        if (data[rowx]) {
            db.collection("index_repo_campaign").update({ "_id": data[rowx][1] }, {
                $set: { "readindex": 'R', "readindexdate": new Date() }
            });// หยิบแล้ว stamp ทันที

            rowx++;
            await updateStatus(data, rowx)
        } else {
            db.collection("index_repo_campaign").update({ "_id": data }, {
                $set: { "readindex": 'R', "readindexdate": new Date() }
            });// หยิบแล้ว stamp ทันที

        }
    }

    function requestCrawler(result, rowx, model) {

        try {

            var yUrl = [];
            var fbPostUrl = [];
            var fbPageUrl = [];
            var webUrl = [];
            var panUrl = [];
            var twPostUrl = [];
            var igPostUrl = [];
            var igPageUrl = [];

            var dataloop = 0;
            var loopto = 0
            var countloop = 0
          
            var looppost = 0
            for (const row of result) {
                if (model == "facebook" || model == "instagram" || model == "twitter") {
                    if (row.platform_typepost === "posts") {
                        looppost++
                    }
                }
            }
            if (model == "facebook") {
                if (looppost > 700){
                    loopto = 200
                } else  if (looppost > 500){
                    loopto = 100
                } else  if (looppost > 300){
                    loopto = 60
                } else {
                    loopto = 50
                }
                
            } else if (model == "pantip") {
                loopto = 20
            } else if (model == "instagram") {
                loopto = 10
            } else if (model == "twitter") {
                loopto = 10
            }
            let lengthResult = result.length
            if (looppost > 0) {
                lengthResult = looppost
            }  


            var dataloop = Math.ceil(lengthResult / loopto);

            var loop = Math.floor(lengthResult / dataloop)

            var datatoLoop = loop * dataloop
            var countloops = 0;
            // var countA =  Math.ceil(result/50);
            // console.log(lengthResult)
            // console.log(dataloop)
            // console.log(loop)
            // console.log(datatoLoop)
            var typeLoop = [{ twPost: 0, instagramPage: 0, instagramPost: 0, facebookPage: 0, facebookPost: 0, webUrl: 0, panUrl: 0, yUrl: 0 }]
            var ig = 1; var tw = 1;
            for (const row of result) {

                var date_now = Date.now();
                // console.log(date_now);

                var collect_minute = row.collect_minute;
                collect_minute = (collect_minute != '') ? parseInt(collect_minute) : 60;
                var summiliseccond = collect_minute * 60000;
                var timetostamp = parseInt(date_now) + summiliseccond;

                switch (row.platform) {
                    case "youtube":
                        // updateStatus(row._id)
                        var videoid = row.url.split("=")
                        var a = [row.url, row._id, videoid[1], timetostamp]
                        yUrl.push(a);

                        if (yUrl.length == 1) {

                            updateStatus(yUrl, 0)
                            youtube.getCommentThread(yUrl, 0);//ส่งไป Crawler

                            typeLoop[0].yUrl = typeLoop[0].yUrl + yUrl.length;
                            yUrl = [];

                        }

                        break;
                    case "facebook":

                        var url = row.url
                        // if (typeof url !== "undefined" && newdata.platform_typepost==="posts") {
                        //     console.log("posts");
                        //     facebookcrawler.getSinglePostFromScheduling(url,newdata._id,timetostamp);//ส่งไป Crawler เอา voice
                        // }else if (typeof url !== "undefined" && newdata.platform_typepost==="pages") {
                        //     console.log("pages");
                        //     facebookcrawler.getPost(url,newdata._id,timetostamp);//ส่งไป Crawler เอาอินเด็กลง campagin
                        // }
                        var a = [url, row._id, timetostamp]
                        // var a = ["https://www.facebook.com/LaLiga/videos/live-the-mcdonalds-virtual-laliga-esports-/1751398894896662/",17909,timetostamp]

                        if (row.platform_typepost === "posts") {

                            // updateStatus(row._id)
                            fbPostUrl.push(a)



                            if (fbPostUrl.length == loop) {
                                updateStatus(fbPostUrl, 0)
                                facebookcrawler.getSinglePostFromScheduling(fbPostUrl, 0, true);
                                countloops = countloops + fbPostUrl.length
                                typeLoop[0].facebookPage = typeLoop[0].facebookPage + fbPostUrl.length;
                                fbPostUrl = []
                                countloop++

                            }

                            //  var s = result.length - dataloop 
                            if (dataloop != 1) {
                                if (countloop == dataloop - 1) {
                                    var s = lengthResult - countloops
                                    loop = s;
                                }
                            }

                        }

                        // typeLoop[0].facebookPage = typeLoop[0].facebookPage+1;
                        // else if(row.platform_typepost==="pages") {

                        //     fbPageUrl.push(a)
                        //     if(fbPageUrl.length == 3){
                        //         facebookcrawler.getPost(fbPageUrl);
                        //         fbPageUrl = []
                        //     }

                        // } 

                        break;

                    case "pantip":



                        var a = [row.url, row._id, "pantip.com", timetostamp]
                        panUrl.push(a)
                        // updateStatus(row._id)
                        if (panUrl.length == loop) {
                            updateStatus(panUrl, 0)
                            console.log("Start pantip");
                            pantip.getComment(panUrl, 0, true);//ส่งไป Crawler
                            countloops = countloops + panUrl.length
                            typeLoop[0].panUrl = typeLoop[0].panUrl + panUrl.length;
                            panUrl = []
                            countloop++
                        }
                        // var s = result.length - dataloop 
                        if (dataloop != 1) {
                            if (countloop == dataloop - 1) {
                                var s = lengthResult - countloops
                                loop = s;
                            }
                        }



                    case "twitter":

                        var a = [row.url, row._id, timetostamp]
                        if (row.platform_typepost === "posts") {

                            // updateStatus(row._id)
                            twPostUrl.push(a)
                            if (twPostUrl.length == loop) {

                                updateStatus(twPostUrl, 0)
                                twitter.getTwitterLanding(twPostUrl, 0, true);
                                countloops = countloops + twPostUrl.length

                                typeLoop[0].twPost = typeLoop[0].twPost + twPostUrl.length;
                                twPostUrl = []
                                tw++;

                                countloop++
                            }
                            // var s = result.length - dataloop /
                            if (dataloop != 1) {
                                if (countloop == dataloop - 1) {
                                    var s = lengthResult - countloops
                                    loop = s;
                                }
                            }
                        }
                        break;
                    case "instagram":

                        var a = [row.url, row._id, timetostamp]
                        // var a = ["https://www.facebook.com/LaLiga/videos/live-the-mcdonalds-virtual-laliga-esports-/1751398894896662/",17909,timetostamp]

                        if (row.platform_typepost === "posts") {

                            // updateStatus(row._id)
                            igPostUrl.push(a)
                            if (igPostUrl.length == loop) {

                                updateStatus(igPostUrl, 0)
                                instagram.getCommentInMediaPup(igPostUrl, ig, 0, true);
                                countloops = countloops + twPostUrl.length
                                typeLoop[0].instagramPost = typeLoop[0].instagramPost + igPostUrl.length;
                                igPostUrl = []
                                ig++;
                                countloop++
                            }
                            if (dataloop != 1) {
                                if (countloop == dataloop - 1) {
                                    var s = lengthResult - countloops
                                    loop = s;
                                }
                            }

                        }
                        // else if(row.platform_typepost==="pages") {

                        //     igPageUrl.push(a)
                        //     if(igPageUrl.length == 10){
                        //         instagram.getIndexInMedia(igPageUrl);
                        //         igPageUrl = []
                        //     }
                        //     typeLoop[0].instagramPage = typeLoop[0].instagramPage+1;
                        // } 
                        break;
                    case "website":
                        var url = row.url

                        updateStatus(row._id)
                        if (typeof url !== "undefined") {
                            console.log(url);
                            // setTimeout(() => {
                                
                            webcrawler.getWebCrawler(url, row.domain, row._id, timetostamp);//ส่งไป Crawler
                            // }, 200);
                        }
                        typeLoop[0].webUrl = typeLoop[0].webUrl + 1;
                        break;
                    default:
                        break;
                }
            }

            var d = "finish scheduling"
            if (model == "website") {

                d += " webUrl :" + typeLoop[0].webUrl
            } else if (model == "instagram") {

                d += " instagramPost :" + typeLoop[0].instagramPost
            } else if (model == "facebook") {

                d += " facebookPage :" + typeLoop[0].facebookPage
            } else if (model == "pantip") {

                d += " pantipUrl :" + typeLoop[0].panUrl
            } else if (model == "youtube") {

                d += " youtubeUrl :" + typeLoop[0].yUrl
            } else if (model == "twitter") {

                d += " twUrl :" + typeLoop[0].twPost
            }
            // d += " instagramPage :"+typeLoop[0].instagramPage

            console.log(d);



            callback(null, d);

        }
        catch (err) {
            console.error("ARRAY INDEX BEFORE CRAW : " + err.message);
        }



    }






}



